import { createStore, applyMiddleware, compose, Store, Action } from "redux";
import thunk from 'redux-thunk';
import logger from 'redux-logger';

const composeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const getStore = <StoreStateType, CustomAction extends Action<any>>(reducer: any): Store<StoreStateType, CustomAction> => {
    return createStore(
        reducer,
        composeEnhancers(applyMiddleware(...[thunk, logger]))
    );
};