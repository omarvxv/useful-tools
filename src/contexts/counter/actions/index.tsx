import * as counterActions from './counter';
import { bindActionCreators } from 'redux';
import { useCounterDispatch } from '..';

export const useCounterActions = (neededActions?: any) => {
    const dispatch = useCounterDispatch();

    return Object.fromEntries(Object.entries(counterActions)
        .filter(([name]: any) => neededActions ? neededActions.includes(name) : true)
        .map(([name, action]: any) => [name, bindActionCreators(action, dispatch)]));
};