import React from "react";
import { INCREMENT, DECREMENT } from "../consts";

export interface Action {
  type: string;
}

export type TCounter = React.MouseEvent<HTMLButtonElement, MouseEvent>;

export function increment(event: TCounter): Action{
  return {
    type: INCREMENT
  }
};

export function decrement(event: TCounter): Action{
  return {
    type: DECREMENT
  }
};