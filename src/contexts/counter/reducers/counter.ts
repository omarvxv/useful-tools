import { Action } from "../actions/counter"
import { INCREMENT, DECREMENT } from '../consts';

export type CounterStateType = { counter: number }
const initialState: CounterStateType = { counter: 0 }

export function counter(state: CounterStateType = initialState, action: Action): CounterStateType {
    switch (action.type) {
        case INCREMENT:
            return {
                counter: state.counter + 1
            }
        case DECREMENT:
            return {
                counter: state.counter - 1
            }
        default:
            return state
    }
}