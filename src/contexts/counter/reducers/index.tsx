import { combineReducers } from "redux";
import { counter, CounterStateType as CounterType } from "./counter";

export interface RootState {
  counter: CounterType;
}

export const rootReducer = combineReducers<RootState>({ counter });
