import * as React from 'react';
import {
    Provider,
    ReactReduxContextValue,
    createDispatchHook,
    createSelectorHook
} from 'react-redux';
import { getStore } from '../utils';
import { rootReducer } from './reducers';
import { CounterStateType } from './reducers/counter';
import { Action } from './actions/counter';

const store = getStore<CounterStateType, Action>(rootReducer);

const context = React.createContext<ReactReduxContextValue<CounterStateType, Action>>({
    store,
    storeState: { counter: 0 }
});

export const CounterProvider: React.FC = ({ children }) => (
    <Provider store={store} context={context}>
        {children}
    </Provider>
)

export const useCounterSelector = createSelectorHook(context);

export const useCounterDispatch = createDispatchHook(context);