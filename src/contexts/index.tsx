import * as React from 'react';
import { CounterProvider } from './counter/index';
import { AuthProvider } from './auth';

export const ContextProvider: React.FC = ({ children }) => (
    <CounterProvider>
        <AuthProvider>
            {children}
        </AuthProvider>
    </CounterProvider>
);