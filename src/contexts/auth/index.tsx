import * as React from 'react';
import { Provider, createSelectorHook, createDispatchHook, ReactReduxContextValue } from 'react-redux';
import { getStore } from '../utils';
import { rootReducer } from './reducers';
import { initialAuthState } from './reducers/auth';
import { IAuth } from './reducers/auth';
import { Action } from 'redux';

const store = getStore<IAuth, Action>(rootReducer);
const context = React.createContext<ReactReduxContextValue<IAuth, Action>>({
    store,
    storeState: { ...initialAuthState }
});

export const AuthProvider: React.FC = ({ children }) => (
    <Provider store={store} context={context}>
        {children}
    </Provider>
);

export const useAuthSelector = createSelectorHook(context);

export const useAuthDispatch = createDispatchHook(context);

