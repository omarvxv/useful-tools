import { Action } from "redux";
import * as actionTypes from '../consts';

export type IAuth = { authorized: boolean | string, name: string | null };

export const initialAuthState: IAuth = { authorized: false, name: null };

export const auth = (state: IAuth = initialAuthState, action: Action & { payload?: { name: string } }): IAuth => {
    switch (action.type) {
        case actionTypes.AUTHORIZE:
            return {
                authorized: true,
                name: action.payload!.name
            };
        case actionTypes.UNAUTHORIZE:
            return {
                authorized: false,
                name: null
            };
        case actionTypes.IN_PROCESS:
            return {
                authorized: "loading",
                name: null
            }
        default:
            return state;
    };
};