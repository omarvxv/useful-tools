import { combineReducers } from 'redux';
import { auth, IAuth } from './auth';

export interface RootAuthState {
    auth: IAuth
}

export const rootReducer = combineReducers<RootAuthState>({ auth });