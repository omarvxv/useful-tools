import * as actionTypes from '../consts';
import { Action } from 'redux';
import { ThunkAction } from 'redux-thunk';

const authorize = (): Action & { payload: { name: string } } => ({
    type: actionTypes.AUTHORIZE,
    payload: {
        name: getUserName()
    }
});

const unauthorize = (): Action => ({
    type: actionTypes.UNAUTHORIZE
});

const inProcess = (): Action => ({
    type: actionTypes.IN_PROCESS
})

export const connection = (): ThunkAction<any, any, any, Action> => dispatch => {
    dispatch(inProcess);

    return window.gapi.load('client:auth2', (): void => {
        window.gapi.client.init({
            clientId: '979793352731-1cgf90ol4qrsst2gm0qg0o48n3dvoe3d.apps.googleusercontent.com',
            scope: 'email'
        })
            .then(() => {
                const isSignedIn = window.gapi.auth2.getAuthInstance().isSignedIn;
                isSignedIn.get() && dispatch(authorize);
                isSignedIn.listen(() => dispatch(isSignedIn.get() ? authorize() : unauthorize()));
            });
    });
};

const getUserName = (): string => {
    return window.gapi.auth2.getAuthInstance().currentUser.get().getBasicProfile().getGivenName();
}
