import * as AuthActions from './auth';
import { bindActionCreators } from 'redux';
import { useAuthDispatch } from '../';

interface IBoundedAuthActions {
    connection: () => Promise<void>;
    setName: () => void
}

export const useAuthAction = (neededActions?: (keyof typeof AuthActions)[]) => {
    const dispatch = useAuthDispatch();

    return Object.fromEntries(Object.entries(AuthActions)
        .filter(([name]) => neededActions ? neededActions.includes(name as keyof typeof AuthActions) : true)
        .map(([name, action]) => [name, bindActionCreators(action, dispatch)]));
};