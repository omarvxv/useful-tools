import React from "react";
import { useCounterActions } from "./contexts/counter/actions";
import { useCounterSelector } from "./contexts/counter";
import { RootState } from "./contexts/counter/reducers";

const Counter: React.FC = () => {
  const { increment, decrement } = useCounterActions(['increment', 'decrement']);
  const counter = useCounterSelector<RootState, number>(state => state.counter.counter);

  return (
    <>
      <button onClick={increment} className="button">
        increment
      </button>
      <span className="counter">{counter}</span>
      <button onClick={decrement} className="button">
        decrement
      </button>
    </>
  );
};

export default Counter;