import React from 'react';
import Counter from './Counter';
import { GoogleAuth } from './GoogleAuth';

const App: React.FC = () => {
  return (
    <div className="container">
      <div className="row">
        <div className="col">
          <GoogleAuth />
        </div>
      </div>
      <div className="row">
        <div className="col">
          <Counter />
        </div>
      </div>
    </div>
  );
}

export default App;
