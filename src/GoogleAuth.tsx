import * as React from 'react';
import { useAuthAction } from './contexts/auth/actions';
import { useAuthSelector } from './contexts/auth';
import { RootAuthState } from './contexts/auth/reducers'

export const GoogleAuth: React.FC = () => {
    const { connection } = useAuthAction();
    const { authorized, name } = useAuthSelector((state: RootAuthState) => state.auth);

    React.useEffect(() => { connection(); }, []);

    function renderAuthStatus(): React.ReactNode {
        switch (authorized) {
            case true:
                return (
                    <button className="btn btn-danger" onClick={() => gapi.auth2.getAuthInstance().signOut()}>
                        Sign out
                    </button>
                );
            case 'loading':
                return (
                    <button className="btn btn-primary" type="button" disabled>
                        <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                        <span className="sr-only">Loading...</span>
                    </button>
                )
            default:
                return (
                    <button className="btn btn-primary" onClick={() => gapi.auth2?.getAuthInstance().signIn()}>
                        Sign in
                    </button>
                );
        }
    }

    return (
        <div className="row" style={{ alignItems: "center" }}>
            {authorized === true && <>Hi, {name}</>}
            <div className="col">
                {renderAuthStatus()}
            </div>
        </div>
    );
}